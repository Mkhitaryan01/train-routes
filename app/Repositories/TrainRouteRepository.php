<?php

namespace App\Repositories;

class TrainRouteRepository implements TrainRouteRepositoryInterface
{
    /**
     * get response
     * @param $soapClient
     * @param $request
     * @param $auth
     * @return mixed|string
     */
    public function getResponseByRequest($soapClient, $request, $auth){
        $train = trim($request['train']);
        $info = [
            'from' => trim($request['from']),
            'to' => trim($request['to']),
            'day' => trim($request['day']),
            'month' => trim($request['month'])
        ];
        $param = array(
            $auth, $train, $info
        );
        try {
            $response = $soapClient->__soapCall('trainRoute', $param);
            return json_encode(array('response' => $response->route_list->stop_list));
        } catch (Exception $e) {
            return $e->getMessage() ."\n";
        }
    }
}
