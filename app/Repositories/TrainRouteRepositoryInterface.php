<?php

namespace App\Repositories;

interface TrainRouteRepositoryInterface{
    /**
     * @param $soapClient
     * @param $request
     * @param $auth
     * @return mixed
     */
    public function getResponseByRequest($soapClient, $request, $auth);
}
