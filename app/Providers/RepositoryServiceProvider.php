<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\TrainRouteRepositoryInterface::class, \App\Repositories\TrainRouteRepository::class);
        $this->app->bind(\App\Services\TrainRouteServiceInterface::class, \App\Services\TrainRouteService::class);
    }
}
