<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrainRouteRequest;
use App\Services\TrainRouteServiceInterface;

class TrainRouteController extends Controller
{
    /**
     * @var TrainRouteServiceInterface
     */
    protected $trainRouteService;

    /**
     * TrainRouteController constructor.
     * @param TrainRouteServiceInterface $trainRouteService
     */
    public function __construct(TrainRouteServiceInterface $trainRouteService)
    {
        $this->trainRouteService = $trainRouteService;
    }

    /**
     * @param TrainRouteRequest $request
     * @return $this
     */
    public function index(TrainRouteRequest $request) : Object
    {
        $trainRoutes = $this->trainRouteService->getTrainRoutes();
        return response($trainRoutes, 200)
            ->header('Content-Type', 'json');
    }

}
