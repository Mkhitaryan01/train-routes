<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainRouteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'train' => ['required', 'max:20'],
            'to' => ['required', 'max:255'],
            'from' => ['required', 'max:255'],
            'day' => ['required'],
            'month' => ['required']
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'train.required' => 'Необходимо заполнить это поле.',
            'to.required' => 'Необходимо заполнить это поле.',
            'from.required' => 'Необходимо заполнить это поле.',
            'day.required' => 'Необходимо заполнить это поле.',
            'month.required' => 'Необходимо заполнить это поле.'
        ];
    }
}
