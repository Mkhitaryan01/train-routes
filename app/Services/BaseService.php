<?php

namespace App\Services;

use Illuminate\Http\Request;

class BaseService
{
    private $request;

    /**
     * BaseService constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    protected function getRequest() : Request
    {
        return $this->request;
    }

    /**
     * @return array
     */
    protected function getParams(): array
    {
        return $this->request->all();
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    protected function getParam($key, $default = null)
    {
        return $this->request->get($key,$default);
    }
}
