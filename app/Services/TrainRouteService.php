<?php

namespace App\Services;

use App\Repositories\TrainRouteRepositoryInterface;
use Illuminate\Http\Request;
use SoapClient;

class TrainRouteService extends BaseService implements TrainRouteServiceInterface
{
    /**
     * @var TrainRouteRepositoryInterface
     */
    protected $trainRouteRepository;

    /**
     * TrainRouteService constructor.
     * @param Request $request
     * @param TrainRouteRepositoryInterface $trainRouteRepository
     */
    public function __construct(Request $request, TrainRouteRepositoryInterface $trainRouteRepository)
    {
        parent::__construct($request);
        $this->trainRouteRepository = $trainRouteRepository;
    }

    /**
     * @return mixed
     */
    public function getTrainRoutes(){
        $api_url = config('trainRoute.api_url');
        $params = array(
            'trace'    => true,
            'encoding' => 'UTF-8'
        );
        $soapClient = $this->getSoapClient($api_url, $params);
        $request = $this->getParams();
        $auth = config('trainRoute.auth');
        return $this->trainRouteRepository->getResponseByRequest($soapClient, $request, $auth);
    }

    /**
     * get soap api by api url and params
     * @param $api_url
     * @param $params
     * @return mixed|SoapClient
     */
    public function getSoapClient($api_url, $params){
        return new SoapClient($api_url, $params);
    }
}
