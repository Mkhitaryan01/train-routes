<?php

namespace App\Services;

interface TrainRouteServiceInterface{
    /**
     * @return mixed
     */
    public function getTrainRoutes();

    /**
     * @param $api_url
     * @param $params
     * @return mixed
     */
    public function getSoapClient($api_url, $params);
}
